package ru.sag.baggage;

public class Baggage {
    private String surname;
    private int amount;
    private int weight;

    Baggage(String surname, int amount, int weight) {
        this.surname = surname;
        this.amount = amount;
        this.weight = weight;
    }
@SuppressWarnings("unused")
    public Baggage() {
        this("Не указано", 0, 0);
    }

    String getSurname() {
        return surname;
    }
    @SuppressWarnings("unused")
    public int getAmount() {
        return amount;
    }
    @SuppressWarnings("unused")
    public int getWeight() {
        return weight;
    }
    @SuppressWarnings("unused")
    boolean Clad() {
        return amount == 1 && weight <= 10;
    }
}
