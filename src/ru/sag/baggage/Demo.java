package ru.sag.baggage;

public class Demo {
    public static void main(String[] args) {

        Baggage bag1 = new Baggage("Шмидт", 5, 30);
        Baggage bag2 = new Baggage("Сергеева", 1, 5);
        Baggage bag3 = new Baggage("Капралов", 3, 7);
        Baggage bag4 = new Baggage("Бирюков", 1, 3);
        Baggage bag5 = new Baggage("Антонов", 7, 10);

        Baggage[] baggages = {bag1, bag2, bag3, bag4, bag5}; //массив багажей

        pasClad(baggages); //вызываем метод
    }

    private static void pasClad(Baggage[] baggages) {
        for (Baggage baggage : baggages) {
            if (baggage.Clad()) {
                System.out.println(baggage.getSurname() + " - С ручным кладом");
            }
        }
    }
}
