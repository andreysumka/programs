package ru.sag.book;

@SuppressWarnings("unused")
public class Book {
    private String author;
    private String name;
    private int year;

    Book(String author, String name, int year) {
        this.author = author;
        this.name = name;
        this.year = year;
    }


    Book() {
        this("неизвестно", "неизвестно", 0);
    }


    public String getAuthor() {
        return author;
    }


    public String getName() {
        return name;
    }

    int getYear() {
        return year;
    }

    @Override
    public String toString() {
        return "Книга (" +
                "Автор - " + author +
                ", фамилия - " + name +
                ", год издания - " + year +
                ')';
    }

    boolean isSameYears(Book book) {
        return this.year == book.year;
    }
}
