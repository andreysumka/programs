package ru.sag.book;

public class Demo {
    public static void main(String[] args) {
        Book book1 = new Book("Толкин", "Властелин колец", 2010);
        Book book2 = new Book("Киплинг", "Маугли", 1935);
        Book book3 = new Book("Кинг", "Сияние", 2018);
        Book book4 = new Book("Пушкин", "Золотая рыбка", 2018);
        Book book5 = new Book("Толстой", "Война и Мир", 2018);

        Book[] books = {book1, book2, book3, book4, book5};
        yearsBook2018(books);
        sameYears(book1, book2);
    }

    private static void yearsBook2018(Book[] books) {
        for (Book book : books) {
            if (book.getYear() == 2018) {
                System.out.println(book);
            }
        }
    }

    private static void sameYears(Book book1, Book book2) {
        if (book1.isSameYears(book2)) {
            System.out.println("Книга(и) не относятся к данной категории  ");
        } else {
            System.out.println("Книга(и) относятся к данной категории ");
        }
    }
}
