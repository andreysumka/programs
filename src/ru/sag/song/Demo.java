package ru.sag.song;

import java.util.Scanner;

/**
 * Класс для реализации Main
 * Вывод песен с заданной продолжительности
 * Вывод песен относящейся к категории Short
 * Сравнение первой песни и последней
 *
 * @author Andrey Sumka 18it18
 */
public class Demo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        Song song1 = new Song("False Alarm", "The Weekend", 120);
        Song song2 = new Song("Old Town Road", "Lil Nas X", 60);
        Song song3 = new Song("Gucci Gang", "Lil Pump", 430);
        Song song4 = new Song("Circles", "Post Malone", 120);
        Song song5 = new Song("Trap Luv", "Big Baby Tape", 90);

        Song[] songs = {song1, song2, song3, song4, song5};//массив песен

        System.out.println("Введите длину песни: ");
        int songIn = sc.nextInt();
        infOfSong(songs, songIn);//вывод метода

        System.out.println("Информация о самой короткой песне: ");
        infOfShortSong(songs);//вывод метода

        SameCategory(songs);//вывод метода
    }

    /**
     * Метод вывода информации о песнях
     *
     * @param songs массив песен
     * @param songI Вывод песен
     */
    private static void infOfSong(Song[] songs, int songI) {
        int count = 0;
        for (Song song : songs) {
            if (song.getTimeOfSong() == songI) {
                System.out.println(song);
                count++;
            }
        }
        if (count == 0) {
            System.out.println("Нет таких песен :(");
        }
    }

    /**
     * Метод поиска песен с категорией Short
     *
     * @param songs массив песен
     */
    private static void infOfShortSong(Song[] songs) {
        for (Song song : songs) {
            if (song.Category().equals("Short")) {
                System.out.println(song);

            }
        }
    }

    /**
     * Метод проверки категории первой и последней песни
     *
     * @param songs Массив песен
     */
    private static void SameCategory(Song[] songs) {
        if (songs[0].isSameCategory(songs[4])) {
            System.out.println("Песни не относятся к данной категории  ");
        } else {
            System.out.println("Песни относятся к данной категории ");
        }
    }
}
