package ru.sag.song;

/**
 *
 * Класс сведений о песне
 *
 * @author Andrey Sumka 18it18
 */
public class Song {
    private String nameOfSong;
    private String authorOfSong;
    private int timeOfSong;

    Song(String nameOfSong, String authorOfSong, int timeOfSong) {
        this.nameOfSong = nameOfSong;
        this.authorOfSong = authorOfSong;
        this.timeOfSong = timeOfSong;
    }

    @SuppressWarnings("unused")
    public Song() {
        this("Не указана", "Не указан", 0);
    }

    @SuppressWarnings("unused")
    public String getNameOfSong() {
        return nameOfSong;
    }

    @SuppressWarnings("unused")
    public String getAuthorOfSong() {
        return authorOfSong;
    }

    int getTimeOfSong() {
        return timeOfSong;
    }

    @Override
    public String toString() {
        return "Песня (" +
                "Название песни - " + nameOfSong +
                ", автор песни - " + authorOfSong +
                ", длительность - " + timeOfSong + "сек." +
                ')';
    }

    /**
     * Метод проверки категории песен
     *
     * @return категория песни
     */
    String Category() {

        String category = "";

        if (timeOfSong < 120) {
            category = "Short";
        }

        if (timeOfSong > 120 && timeOfSong < 240) {
            category = "Medium";
        }

        if (timeOfSong > 240) {
            category = "Long";
        }
        return category;
    }

    /**
     * Метод получения текущей и полученной песни
     *
     * @param song песня
     * @return сравнение текущей и полученной песни
     */
    boolean isSameCategory(Song song) {
        return this.Category().equals(song.Category());
    }
}
